package com.easwareapps.baria.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.easwareapps.baria.R;
import com.easwareapps.baria.utils.BariaPref;
import com.easwareapps.baria.utils.PInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.ViewHolder> {


    Context context;
    ArrayList<File> dirs;
    static DirectoryAdapter instance = null;
    private DirectoryAdapter directoryAdapter = null;
    private  RecyclerView dirList;
    private String currentPath;
    private Toolbar toolbar;

    public static DirectoryAdapter getInstance(Context context, RecyclerView rv, Toolbar toolbar) {
        //rv = rv1;
        if(instance == null) {
            instance = new DirectoryAdapter();
        }
        instance.context = context;
        instance.dirList = rv;
        instance.toolbar = toolbar;

        BariaPref pref = BariaPref.getInstance(context);
        String defaultPath = pref.getBackupDir();
        instance.updateDirectoryList(defaultPath);
        return instance;

    }

    public void updateDirectoryList(String path) {
        if(dirs != null) dirs.clear();
        try {
            currentPath = new File(path).getCanonicalPath();
            new ListDir(this).execute(new String[]{path});
        }catch (Exception e) {

        }

    }

    public void goBack() {
        updateDirectoryList(currentPath + "/..");
    }


    public void requestUpdate() {
        notifyDataSetChanged();
    }

    public String addNewDirectory(String name) {

        try {
            File f = new File(currentPath, name);
            if (f.exists()) {
                return context.getResources().getString(R.string.file_exsist);
            } else {
                if (f.mkdirs()) {
                    updateDirectoryList(currentPath);
                    return "true";
                }
            }

        }catch (Exception e){

        }
        return "false";

    }

    public String getCurrentPath() {
        return currentPath;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView icon = null;
        TextView name = null;
        View mainView;
        public ViewHolder(View view){
            super(view);
            mainView = view;
            icon = (ImageView)view.findViewById(R.id.app_icon);
            name = (TextView)view.findViewById(R.id.app_name);
            mainView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            gotoDirectory(dirs.get(getAdapterPosition()).getAbsolutePath());
        }
    }

    private void gotoDirectory(String path) {
        updateDirectoryList(path);
        requestUpdate();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dir_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DirectoryAdapter.ViewHolder holder, int position) {
        holder.name.setText(dirs.get(position).getName());

    }


    @Override
    public int getItemCount() {
        return dirs.size();
    }

    class ListDir extends AsyncTask<String, Void, Void> {
        DirectoryAdapter da;
        public ListDir (DirectoryAdapter ds) {
            this.da = ds;
        }
        @Override
        protected Void doInBackground(String... dir) {
            dirs = new ArrayList<>();
            String path = (dir.length == 0)?null:dir[0];
            getDirectories(path);
            return null;
        }

        private void getDirectories(String path) {

            Log.d("Files", "Path: " + path);
            File directory = new File(path);
            File files[] = directory.listFiles();
            if(files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        dirs.add(file);
                    }
                }
            }
            Collections.sort(dirs, new Comparator<File>() {

                public int compare(File p1, File p2) {
                    return p1.getName().compareToIgnoreCase(p2.getName());
                }
            });

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dirList.setAdapter(da);
            try {
                String relativePath =
                        currentPath.replace(
                                Environment.getExternalStorageDirectory()
                                        .getCanonicalPath() + "/", "");
                if(relativePath.equals(Environment.getExternalStorageDirectory())) {
                    relativePath = "";
                }
                toolbar.setTitle(relativePath);

            } catch (Exception e) {

            }
            da.requestUpdate();


        }

    }

    public boolean isAtRoot() {
        try {
            return currentPath.equals(Environment.getExternalStorageDirectory().getCanonicalPath());
        }catch (Exception e) {

        }
        return false;
    }

}
